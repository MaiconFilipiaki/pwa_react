import React, { Suspense } from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

const LoginLazy = React.lazy(() => import('./Login'))
const AppLazy = React.lazy(() => import('./App'))

export default () => (
    <Suspense fallback={<div>Carregando...</div>}>
        <BrowserRouter>
            <Switch>
                <Route path="/login" component={LoginLazy} />
                <Route path="/" component={AppLazy} />
            </Switch>
        </BrowserRouter>
    </Suspense>
)
