import React from 'react';
import { NavLink } from 'react-router-dom';

function App() {
  return (
    <>
      <h1>App</h1>
      <NavLink to="/login">LOGIN</NavLink>
    </>
  );
}

export default App;
